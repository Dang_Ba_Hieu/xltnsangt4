"""-------------------------------------------------------------------------------------
                            Bai tap 3
                    SV thuc hien: Dang Ba Hieu
                        MSSV: N16DCDT032
-------------------------------------------------------------------------------------"""
# Bai1:
print("Bai1: Viet 1 ham de quy co ten sum_deep() truyen vao l1 tinh tong cac so trong day")
l1=[3,4,'hello',(4.6,5.6),[3,7,2,2,[0.3,5],'text'],6,3,3.5]
l2=[]
# Ham de quy tim trong list l1 loc cac phan tu la SO sang l2:
def checklist(l1):
    for i in l1:
        if ((type(i) == int) or (type(i) == float)):
            l2.append(i)

        if((type(i)==list)or(type(i)==tuple)):
            checklist(i)
#Ham de quy tinh tong cac thanh phan cua list l
def sum_deep(l):
    if not l:
        return 0
    else:
        return l[0]+sum_deep(l[1:])

checklist(l1)
print("l1= ", l1)
print("l2= ", l2)
l1=l2
print("Sum of all element in list l1: ",sum_deep(l1))

# Bai2:
print("\nBai2: Viet ham tra ve 1 danh sach cua cac so co gia tri Max 2 day ")
l1=[2,4,6,4,9,4,7]
l2=[5,3,5,6,9,7,5]
l3=[]
# Ham kiem tra tat ca cac so trong day
def detectNumber(l):
    for ele in l:
        if (type(ele) != int):
            if (type(ele) != float):
                return False
    return True
# Ham tra ve list co gia tri cao nhat cua 2 list co do dai bat ki
def Maxlist(l1,l2):
    if((detectNumber(l1)==True) and (detectNumber(l2)==True)):
        x=min(len(l1),len(l2))
        y=max(len(l1),len(l2))
        for i in range(x):
            l3.append(max(l1[i],l2[i]))
        x=min(len(l1),len(l2))
        #so sanh gia tri 2 list, neu len l1< len l2 thi in tiep l2 va nguoc lai
        if(len(l1)<len(l2)):
            for x in range(x,y):
                l3.append(l2[x])
        if(len(l1)>len(l2)):
            for x in range(x,y):
                l3.append(l1[x])
        return l3;
    else:
        print("list 1 hoac list 2 co phan tu khong phai la so!")
print("l1=",l1,"\nl2=",l2)
l3=Maxlist(l1,l2)
print("Maxlist l3=",l3)

# Bai3:
print("\nBai3: ")
f=open("word_count_data.txt","r")
def countword(x):
    str1=x.readlines()
    print(str1)
    lismap=list(map(len,str1))
    word=0
    for i in lismap:
        word+=i
    return word-len(str1)+1
print(countword(f))
#------------------------------------END------------------------------------