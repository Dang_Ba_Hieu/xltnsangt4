"""==============Problem2-List=============="""

# a)Remove the item “python” in the list.
print("\nCau a)Xoa : 'python'")
l=[23, 4.3, 4.2, 31,'python', 1, 5.3, 9, 1.7]
l.remove('python')
print("l=",l)

# b)Sort this list by ascending and descending.
print("\nCau b)Sap xep list theo tang dan va giam dan")
l.sort()
print("Tang dan: l=",l)
l.sort(reverse=1)
print("Giam dan: l=",l)

# c)Check either number 4.2 to be in l or not?
print("\nCau c)So 4.2 co nam trong list?")
print(4.2 in l)