"""==============Problem4-Tuples=============="""

# a)Create a tuple t of 4 elements, that are: 1, 'python', [2, 3], (4, 5)
print("Cau a)")
t=(1,'python',[2,3],(4,5))
print(t)
# b)Unpack t into 6 variables, that are: 1, 'python', 2, 3, 4, 5
print("Cau b)")
print("Giai nen t ra 6 bien")
a,b,x,y=(1,'python',[2,3],(4,5))
print(a)
print(b)
c=x[0]
print(c)
d=x[1]
print(d)
e=y[0]
print(e)
f=y[1]
print(f)

# c)Print out the last element of t
print("Cau c)")
print(t[3])

# d)Add t to a list [2, 3]
print("Cau d)")
v=[2,3]
v.append(t)
print(v)
# e)Check whether list [2, 3] is duplicated in t
print("Cau e)")
print([2,3] in t)
# f)Remove list [2, 3] from t
print("Cau f)")
print("Khong xoa duoc vi tuple la kieu immutable")
# g)Convert tuple t into a list
print("Cau g)")
print(list(t))