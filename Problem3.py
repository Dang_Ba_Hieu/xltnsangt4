"""==============Problem3-Set=============="""
# a)Create three sets: A = {1, 2, 3, 4, 5, 7}, B = {2, 4, 5, 9, 12, 24}, C = {2, 4, 8}
print("Cau a) Tao sets A")
A={1,2,3,4,5,7}
B={2,4,5,9,12,24}
C={2,4,8}
print("A= ", A,"\nB= ", B,"\nC= ",C)
# b)Iterate over all elements of set C and add each element to A and B
print("Cau b)")
for s in set(C):
    print(s)
print("Cac phan tu cua C: ",C)
for n in C:
    print(n,"\t")
for n in C:
    print(n)
    A.add(n)
    B.add(n)
print("C=",C)
# c)Print out A and B after adding elements
print("Cau c)")
print("Cac phan tu cua A|C=",A)
print("Cac phan tu cua B|C=",B)

# d)Print out the intersection of A and B
print("Cau d)")
print("Phan tu A&B: ",A&B)

# e)Print out the union of A and B
print("Cau e)")
print("Phan tu A|B: ",A|B)

# f)Print out elements in A but not in B
print("Cau f)")
print("Phan tu A-B: ",A-B)

# g)Print out the length of A and B
print("Cau g)")
print("Do dai set A=",len(A))
print("Do dai set B=",len(B))

# h)Print out the maximum value of A union B
print("Cau h)")
print("MAX A|B= ",max(A|B))

# i)Print out the minimum value of A union B
print("Cau i)")
print("MIN A|B= ", min(A|B))