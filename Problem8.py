""""============Problem8-Random============"""
# a)Randomly generate 1000 numbers on interval [1, 50]
print("Cau a): In ngau nhien 1000 so trong khoang [1,50]")
l=[]
import random
for i in range(1000):
    r=random.randint(1,50)
    l.append(r)
    print(r)
# b)Count the occurence of each number. Print those numbers as well as its
# occurrence to console. Define a pretty format
print("Cau b): ")
print("l=", l)
print("Gia tri va so lan xuat hien:")
for i in range(1,51):
    print(i,"::", l.count(i))
# c)Given a number N, randomly generate a list of positive integer number
# such that sum of values in this list is N.
print("Cau c): ")
print("Nhap vao so N (voi N>0)= ")
N=int(input())
while N>0:
# tao ngau nhien 1 danh sach so nguyen duong
    l1=[]
    while sum(l1)!=N:
        r = random.randint(0, N)
        while r>N:
            r = random.randint(0, N)
        l1.append(r)
        if sum(l1)>N:
            break
    if sum(l1)==N:
        print(l1)
        break
else:
    print("Warning! So ban nhap vao khong hop le!")