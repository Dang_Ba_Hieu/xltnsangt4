"""=============Problem10-MATRIX============="""
# matrix 2x3
A=[[1,2,3],
   [4,5,6]]
# matrix 3x2
B=[[2,3],
   [4,5],
   [6,7]]
C=[]
print("A=", A,"\nB=", B)
print("C=AxB")
import numpy as np
A = np.array([[1,2,3],[4,5,6]])
B = np.array([[2,3],[4,5],[6,7]])
C=A.dot(B)
print("C=",C)
