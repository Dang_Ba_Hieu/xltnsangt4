""""----------------------------------------------------------------------------------------------
                        30 Bai tap ca nhan ve ham Numpy
                            SV thuc hien: Dang Ba Hieu
                                MSSV: N16DCDT032
----------------------------------------------------------------------------------------------"""
# Bai 1: Import the numpy package under the name `np` and print out the numpy version and the configuration"
print("\nBai 1: Khai bao numpy duoi ten 'np',in ra phien ban va cau hinh numpy")
import numpy as np
print("Phien ban numpy: ", np.__version__)
print("Cau hinh: ")
np.show_config()

# Bai 2: Create a null vector of size 10
print("\nBai 2: Tao vector 0 co kich thuoc 10")
A=np.zeros(10)
print('A= ',A)

# Bai 3: How to find the memory size of any array
print("\nBai 3: Tim kich thuoc bo nho cua mang bat ki")
A=np.zeros((4,3))
print('A= ',A)
print("Kich thuoc %d byte" % (A.size*A.itemsize))

# Bai 4: How to get the documentation of the numpy add function from the command line?
print("\nBai 4: Lay tai lieu cua ham add numpy tu dong lenh")
np.info(np.add)

# Bai 5: Create a null vector of size 10 but the fifth value which is 1
print("\nBai 5: Tao 1 vecto null co kich thuoc 10, gia tri thu 5 la 1")
A=np.zeros(10)
A[4]=1
print('A= ',A)

# Bai 6: Create a vector with values ranging from 10 to 49
print("\nBai 6: Tao 1 vector voi gia tri khac nhau tu 10-49")
A=np.arange(10,50)
print("A=",A)

# Bai 7: Reverse a vector (first element becomes last)
print("\nBai 7: Dao nguoc 1 vector(Gia tri dau la cuoi)")
A=np.arange(50)
A1=A[::-1]
print('A= ',A)
print('\nA1=',A1)

# Bai 8: Create a 3x3 matrix with values ranging from 0 to 8
print("\nBai 8: Tao 1 matran 3x3 voi gia tri khac nhau tu 0->8")
A=np.arange(9).reshape(3,3)
print('A= ',A)

# Bai 9: Find indices of non-zero elements from \\[1,2,0,0,4,0\\]
print("\nBai 9: Tim chi so cua phan tu khac 0 tu [1,2,0,0,4,0]")
A=np.nonzero([1,2,0,0,4,0])
print('A= ',A)

# Bai 10: Create a 5x6 zero matrix
print("\nBai 10: Tao ma tran khong 5x6")
A=np.zeros((5,6))
print('A= ',A)

# Bai 11: Create a 3x3 identity matrix
print("\nBai 11: Tao 1 ma tran don vi 3x3")
A=np.eye(3)
print('A= ',A)

# Bai 12: Create a 3x3x3 array with random values
print("\nBai 12: Tao 1 mang 3x3x3 voi gia tri random")
A=np.random.random((3,3,3))
print('A= ',A)

# Bai 13: Create a 10x10 array with random values and find the minimum and maximum values
print("\nBai 13: Tao 1 mang 10x10 gia tri random, tim gia tri min,max")
A=np.random.random((10,10))
print('A= ',A)
print("\nMIN(A)= ",A.min(),"\nMAX(A)=", A.max())

# Bai 14: Create a random vector of size 30 and find the mean value
print("\nBai 14: Tao 1 vector random kich thuoc 30, tim gt trung binh")
A=np.random.random(30)
print('A= ',A)
c=A.mean()
print('Mean value: ',c)

# Bai 15: Create a 2d array with 1 on the border and 0 inside
print("\nBai 15: Tao 1 mang 2D voi 1 bao quanh, 0 o trong")
A=np.ones((10,10))
A[1:-1,1:-1]=0
print('A= \n',A)

# Bai 16: How to add a border (filled with 0's) around an existing array?
print("\nBai 16: Them 1 duong vien 0 voi cac gia tri da ton tai trong mang")
A=np.ones((5,5))
A=np.pad(A,pad_width=1, mode='constant', constant_values=0)
print(A)

# Bai 17: What is the result of the following expression?
print("\nBai 17: Ket qua bieu thuc duoi day la gi")
print('KHONG CO BIEU THUC')

# Bai 18: Create a 5x5 matrix with values 1,2,3,4 just below the diagonal
print("\nBai 18: Tao ma tran 5x5 voi gia tri 1-2-3-4 ben duoi thanh duong cheo")
A=np.diag(1+np.arange(4),k=-1)
print('A=',A)

# Bai 19: Create a 8x8 matrix and fill it with a checkerboard pattern
print("\nBai 19: Tao ma tran 8x8, lap no thanh hinh ban co")
A=np.zeros((8,8),dtype=int)
A[1::2,::2]=1
A[::2,1::2]=1
print('A=',A)

# Bai 20: Consider a (6,7,8) shape array, what is the index (x,y,z) of the 100th element?
print("\nBai 20: Mang hinh(6,7,8),chi so phan tu 100 la")
print(np.unravel_index(99,(6,7,8)))

# Bai 21: Create a checkerboard 8x8 matrix using the tile function
print("\nBai 21: Tao 1 ban co ma tran 8x8 su dung ham tile")
A=np.tile(np.array([[0,1],[1,0]]),(4,4))
print('A=',A)

# Bai 22: Normalize a 5x5 random matrix
print("\nBai 22: Binh thuong hoa ma tran random 5x5")
A=np.random.random((5,5))
A1=(A-np.mean(A))/(np.std(A))
print('\nA=', A)
print('\nA1=', A1)

# Bai 23: Create a custom dtype that describes a color as four unsigned bytes (RGBA)
print("\nBai 23: Tao 1 loai hinh dtype mo ta 4 mau R-G-B-A khong dau")
color = np.dtype([("r", np.ubyte, 1),
                  ("g", np.ubyte, 1),
                  ("b", np.ubyte, 1),
                  ("a", np.ubyte, 1)])

# Bai 24: Multiply a 5x3 matrix by a 3x2 matrix (real matrix product)
print("\nBai 24: Nhan ma tran 5x3 boi ma tran 3x2 (ma tran so thuc)")
A=np.dot(np.ones((5,3)),np.ones((3,2)))
print(A)

# Bai 25: Given a 1D array, negate all elements which are between 3 and 8, in place.
print("\nBai 25: Tao mang 1 chieu, doi dau tat ca cac phan tu trong khoang 3-8")
A=np.arange(11)
A[(3<=A)&(A<=8)] *=-1
print('A=',A)

# Bai 26: Consider an integer vector Z, which of these expressions are legal?
print("\nBai 26: Xet vecto nguyen z, bieu thuc nao cho la hop le")
print('KHONG CO BIEU THUC')

# Bai 27: How to round away from zero a float array ?
print("\nBai 27: Lam tron tu 0 cho 1 mang float")
A=np.random.uniform(-5,+5,5)
print('A=',A)
print('Lam tron A= ', np.copysign(np.ceil(np.abs(A)),A))

# Bai 28: How to find common values between two arrays?
print("\nBai 28: Tim cac gia tri giao giua 2 mang")
A1=np.random.randint(0,10,10)
print('A1=',A1)
A2=np.random.randint(0,10,10)
print('A2=',A2)
print('Cac gia tri giao cua 2 mang: ', np.intersect1d(A1,A2))

# Bai 29: How to ignore all numpy warnings (not recommended)?
print("\nBai 29: Loai bo canh bao numpy: ")
default=np.seterr(all="ignore")
A=np.ones(5)/0
print('A=', A)

# Bai 30: How to get the dates of yesterday, today and tomorrow?
print("\nBai 30: Lay ngay: ")
yesterday=np.datetime64('today','D')-np.timedelta64(1,'D')
today =np.datetime64('today', 'D')
tomorrow=np.datetime64('today','D') + np.timedelta64(1,'D')
print("Yesterday:  ",yesterday,"\nToday:\t\t",today,"\nTomorrow: \t",tomorrow)

"""-----------------------------------END---------------------------------"""